#!/bin/bash

# Bash Shell Script for handling building and the like

# Create Debug directory and its subdirectories
function Setup {
    # If Debug directory doesn't exist create it and subdirectories
    if [ ! -d "./Debug/" ]
    then
       mkdir ./Debug 
       mkdir ./Debug/bin
       mkdir ./Debug/intermediates
    # If Debug exists only create subdirectories
    else
        # Create the  Debug/bin  directory if it doesn't already exist
        if [ ! -d "./Debug/bin/" ]
        then
            mkdir ./Debug/bin
        fi
        # Create the  Debug/intermediate  directory if it doesn't already exist
        if [ ! -d "./Debug/intermediate/" ]
        then
            mkdir ./Debug/intermediate/
        fi
    fi
}

# Parse the input
# Use  ./build.sh help  to see options
for arg in $@
do
case $arg in
    # Build project
    build)
        # Ensure that Debug Directory exists
        Setup
        # Build project
        g++ -o ./Debug/bin/Chess.out ./src/bitboard64.cpp ./src/move.cpp ./src/chessboard.cpp ./src/main.cpp -std=c++17
	    ;;
    # Delete Debug directory
    cleanup)
        if [ -d "./Debug/" ]
        then
            rm -r ./Debug
        fi
        ;;
    # Print help screen
    help)
        echo Usage: ./build.sh [options]
        cat ./tools/help-build.txt
        ;;
    # Create Debug folder and its subfolders
    setup)
        Setup
        ;;
    # Build test project (customize this for your needs)
    testbuild)
        # Ensure that Debug Directory exists
        Setup
        # Build project
        g++ -o ./Debug/bin/Chess.out ./src/bitboard64.cpp ./src/move.cpp ./src/chessboard.cpp ./src/test.cpp -std=c++17
        ;;
esac
done
