TODO
====

General
-------

* [ ] Read about how to do this lol ([this](https://stackoverflow.com/a/502029)
      lists a ~~few~~ bunch of starting points)
* [ ] Structure the project
  * [ ] Learn about C++ project structure so that this doesn't have to be dealt
        with later on (which would be a *huge* pain in the ass, especially
        regarding git)
  * [ ] Maybe move all headers for my own files into src/include?
* [x] At least make a bash script for compiling
  * [x] PowerShell Script
  * [x] Bash
  * [x] Whatever exist on Mac OSX
* [ ] Setup a Makefile / Cmake
  * [ ] LOOK AT OTHER PEOPLES' MAKEFILES
  * [ ] Generation of Visual Studio and Visual Studio Code files
  * [ ] Compilation for Windows
  * [ ] Compilation for Linux
  * [ ] Compilation for Mac
* [ ] Document the API (seriously, do everyone including yourself a favour and
      write a documentation)
  * [ ] Markdown? HTML? Something else entirely? Something (like HTML) that
        could be converted to verious formats including plain text, markdown,
        LaTeX, and what not using already existing software?
* [ ] Git(Lab) setup
  * [ ] .gitignore
    * [ ] LOOK AT HOW OTHER PEOPLE WRITE GITIGNORES
    * [x] visual studio (code) files
    * [x] Generally c/c++ ...stuff
    * [ ] Python stuff (since eventually this will include python integration)
  * [ ] Labels for issues (and the like).\
        E.g. `Bug`, `User Experience`, `platform-Windows`, `Security` etc.\
        As always, look at other projects for guidance (here's
        [vim's list of labels](https://github.com/vim/vim/labels), which has way
        more than I'll be needing anytime soon).

Style and Optimization
----------------------

[*Don't overdo it*](https://stackoverflow.com/a/8848532)

* [x] In `cath::Bitboard::operator[]`: Check if the ternary operation at
      the end is even necesarry or of the function can just
      `return shiftedByte & comparator`\
      -> It wasn't necesarry
* [ ] Maybe `cath::Bitbaord64` can be rewritten to be less... terrible
  * [x] Make non-euclidean division euclidean
* [x] Some of the calculations that avoid branching in
      `cath::ChessBoard::getSlidingMovesDiagonal()` could be done with the index
      of the direction rather than the direction itself which *might* be less
      computationally expensive.
* [ ] In `cath::ChessBoard::getSlidingMovesDiagonal()` in
      [`chessboard.cpp`](src/chessboard.cpp), replace the stacked if statments
      for determining if a piece of the own or opposing color is on the next
      square with a single if statement each (or for both if possible) and some
      pointer arithmetic
* [ ] `cath::move` probably doesn't need to store if an en-passant is possible.
      Not having it store that would save space and reasonalby enable storing
      all possible moves as objects of type `move` and not as `Bitbaords64`s
* [ ] The constructor for `cath::ChessBoard` recalculates its member
      `s_SquaresToEdge` each time it is called. This is unnecesarry though not
      import if only one board exists throughout the lifetime of the program,
      which isn't guaranteed to be the case forever.
* [ ] A lot of the variables in `cath::ChessBoard` that are `int`s could
      probably be `signed char`s
* [ ] Decide whether to call it left, right, up, down or west, east, north,
      south
  * [ ] Look up if there is a consensus about this
* [ ] Find a way to rewrite `cath::ChesBoard::getKingMoves()` and
      `cath::ChesBoard::getKnightMoves()` iteratively

Lay foundation for Engine
--------------------------

* [ ] Board representation
  * [ ] Create Bitboard type
    * [x] Write constructors
    * [x] Implement all approriate operators as found
          [here](https://en.cppreference.com/w/cpp/language/operators)
    * [x] Implement setters
    * [ ] Iterators
    * [ ] What more does a type need?
  * [x] What order should the squares be in?\
        -> LERF
  * [ ] Constructors
    * [x] Default constructor
    * [ ] FEN-string constructor
      * [ ] Legal moves will have to be calculated twice, because of how the
            attack maps work (at the moment)
    * [ ] Binary constructor (this has time, since it involves defining a file
          type, which should be done with the utmost care)
  * [ ] An `onMove` function
* [x] Implement some way to move pieces
* [ ] State of the game
  * [ ] Who's turn is it?
  * [x] Which moves are pseudo-legal?
    * [x] Pawns
    * [x] Rooks
    * [x] Knigts
    * [x] Bishops
    * [x] Queens
    * [x] Kings
  * [ ] Which moves are actually legal?
  * [ ] Check?
    * [ ] Bitwise AND the bitmap for the king and the opposing atk. to determine
          check
    * [ ] What moves are legal *now*?
  * [ ] Mate? (Will probably be a result of having no legal moves after a check)
  * [ ] Stalemate
  * [ ] Keep track of moves (purpose: check for repetition draw)
    * Actually you just need to keep track of the last two moves (I think). If
      the move that is being made is the same as the move two moves ago
      increment a counter. Otherwhise reset it to zero.\
      The only over reason to keep track of the entire game (that I could think
      of) would be retrospective.
  * [ ] Who's ahead?
* [ ] Only allow legal moves
* [ ] Load a game based on a FEN-sring
* [ ] Save a game as a FEN-string
* [ ] Implement loading and saving games from/as binary files that store the
      last n moves, where n is greater or equal to the number of repeated moves
      that cause a repetition draw. (This should always be the preferred and
      thus (for the user) easier method)\
      It could also store more moves, this depends on if I decide to store the
      entire game (see above section)
* [ ] `help` command

The Not-so-Distant Future
-------------------------

* [ ] Integrate the board into an actual GUI (This is for the faaaaaaar future)
  * [ ] Display the board
  * [ ] Moving pieces
  * [ ] Who's turn is it?
  * [ ] Split-screen view\
        Should be optional and swappable (black either right or left) so that
        you could either play sitting besides each other or on opposite ends of
        a tablet (though the latter option would have the issue that one player
        would be seeing their pieces upside down unless one colour gets flipped)
* [ ] Chess clock
* [ ] I could make something that allows you to define a starting position that
      then translates that into an array of unsigned char or a FEN-string that
      can be used for initialization.

Accessibility
-------------

* [ ] Make copies of READMEs with highlighting of keywords
* [ ] Translation of READMEs
* [ ] Translation of in-game text

**(Once GUI exists)**
* [ ] Screen reader support
* [ ] Color blind modes (if reasonably needed)

Move Selection
--------------

Python Integration
------------------
<sup>Seriously, this should come last of all. Or at least after the engine is
*done.* </sup>

The Far Far Future
------------------
* [ ] Make a discord bot that you can play chess against
* [ ] Maybe make the more universal types like `cath::Bitboard64` part of a
      library\
      This would certainly involve a lot of optimization and looking at other
      implementations of custom types (by actual capable programmers), reading
      up on what a type has to do, cleaning up, etc.
