Chess Engine Source Code
========================

File Overview
-------------

Here is a brief overview of what each file in this directory does, since they
might not be self explanatory to everyone. For more detailed information, see
the comment at the beginning of each file

Header files (`*.h`) with the same base name as another file simply contain
declarations for whatever the corresponding `*.cpp` file defines.

| File name         | Description             |
|-------------------|-------------------------|
| bitboard.cpp      | 64-bit bitboard struct  |
| boardenums.h      | Enumerator for squares  |
| chessboard.cpp    | Chess board class       |
| move.cpp          | Move struct             |
