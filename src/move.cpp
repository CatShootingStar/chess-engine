/*
 * Implementation of the cath::Move type
 */

#include "move.h"

namespace cath {

    /*
     * Constructor
     *
     * first -> space that the piece was moved from
     * second -> space that the piece was moved to
     * assumes that the moved piece isn't a pawn
     */
    Move::Move (enumSquare first, enumSquare second)
        : from(first), to(second), enPassantPossible(false) {}

    /*
     * Constructor
     *
     * first -> space that the piece was moved from
     * second -> space that the piece was moved to
     * isPawn -> is the moved piece a pawn? (relevant for en-passant)
     */
    Move::Move (enumSquare first, enumSquare second, bool isPawn)
        : from(first), to(second)
    {
        // If a pawn was moved two squares set enPassantPossible to true
        if (isPawn && (first - second == 2 || first - second == -2))
            enPassantPossible = true;
        else
            enPassantPossible = false;
    }

    /* Copy Constructor */
    Move::Move (const Move& other)
        : from(other.from), to(other.to),
          enPassantPossible(other.enPassantPossible) {}
}
