/*
 * Implementation of the cath::Bitboard64 type
 */

#include <bitset>
#include <iostream>
#include "bitboard64.h"

namespace cath {
    
    /* Default Constructor */
    Bitboard64::Bitboard64 ()
        : m_Board(0) { }

    /* Constructor using a std::uint64_t */
    Bitboard64::Bitboard64 (const std::uint64_t value)
        : m_Board(value) { }

    /* Constructor using eight chars */
    Bitboard64::Bitboard64 (const unsigned char bytes[8])
    {
        m_Board = 0;

        for (int i = 0; i < 7; ++i)
        {
            m_Board += (std::uint64_t)bytes[i] << 8 * i;
        }
    }

    /* Copy Constructor */
    Bitboard64::Bitboard64 (const Bitboard64& original)
        : m_Board(original.m_Board) { }

    /* Bitwise XOR two Bitboards */
    Bitboard64 Bitboard64::operator^ (Bitboard64 other) const
    {
        Bitboard64 result;

        result.m_Board = m_Board ^ other.m_Board;

        return result;
    }

    /* Bitwise XOR equals */
    void Bitboard64::operator^= (const Bitboard64 other)
    {
        m_Board = m_Board ^ other.m_Board;
    }

    /* Bitwise AND two Bitboards */
    Bitboard64 Bitboard64::operator& (Bitboard64 other) const
    {
        Bitboard64 result;
        
        result.m_Board = m_Board & other.m_Board;

        return result;
    }

    /* Bitwise AND equals */
    void Bitboard64::operator&= (const Bitboard64 other)
    {
        m_Board = m_Board & other.m_Board;
    }

    /* Bitwise OR two Bitboards */
    Bitboard64 Bitboard64::operator| (Bitboard64 other) const
    {
        Bitboard64 result;

        result.m_Board = m_Board | other.m_Board;

        return result;
    }

    void Bitboard64::operator|= (const Bitboard64 other)
    {
        m_Board = m_Board | other.m_Board;
    }

    /* Bitwise NOT */
    Bitboard64 Bitboard64::operator~ () const
    {
        Bitboard64 result;

        result.m_Board = ~m_Board;

        return result;
    }

    /* Equals operator */
    bool Bitboard64::operator== (Bitboard64 other) const
    {
        bool equal;

        equal = m_Board == other.m_Board;

        return equal;
    }

    /* Does not equal operator */
    bool Bitboard64::operator!= (Bitboard64 other) const
    {
        return !(*this == other);
    }

    /* Get the state of the bit at index */
    bool Bitboard64::operator[] (int index) const
    {
        std::uint64_t comparator = 0x8000000000000000 >> index; // Shift a bit to the place which shall be inverted

        return m_Board & comparator;
    }

    /*
     * Get the state of the bit at index
     * Identical to operator[]
     */
    bool Bitboard64::state(int index) const
    {
        return (*this)[index];
    }

    /* Invert the bit at index */
    void Bitboard64::invert (const int index)
    {
        std::uint64_t inverter = 0x8000000000000000 >> index;   // Shift a bit to the place which shall be inverted

        m_Board ^= inverter;      // Bitwise XOR the byte and the inverter to inver that bit
    }

    /* Set the bit at index to true */
    void Bitboard64::set (const int index)
    {
        std::uint64_t setter = 0x8000000000000000 >> index;   // Shift a bit to the place which shall be set

        m_Board |= setter;
    }

    /* Reset the bit at index to false */
    void Bitboard64::reset (const int index)
    {
        // Set the bit and then invert it
        set (index);
        invert (index);
    }

    /* Return the total number of set bits */
    int Bitboard64::quantity ()
    {
        int quantity = 0;

        // Add one to the output for each set bit of the bitboard
        for (int i = 0; i < 64; ++i)
        {
            quantity += (*this)[i] ? 1 : 0;
        }
        
        return quantity;
    }

    /* Return the total number of set bits in a Bitbaord64 */
    int quantity (const Bitboard64 board)
    {
        Bitboard64 copyBoard = board;

        return copyBoard.quantity();
    }
}

/* Overriede << operator for ostream */
std::ostream& operator<< (std::ostream& stream, const cath::Bitboard64& bitboard)
{
    stream << "{ " << std::bitset<64>(bitboard.m_Board) << " }";
    
    return stream;
}

#undef BOARD_SIZE
#undef BOARD_SIZE_BITS
