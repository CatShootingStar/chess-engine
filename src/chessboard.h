/*
 * Declaration of the cath::ChessBoard class
 * 
 * Notes about the structure of this
 * ---------------------------------
 * The board will be represented rank by rank and each rank file by file. That
 * means: a1 is at index 0, h1 at index 7, a2 at index 8, h8 at index 63.
 * Apparently this is called LERF (at least in Bibob
 * https://www.chessprogramming.org/Bibob#Square_Mapping_Modes)
 */

#pragma once
#ifndef _CATHBOARD_
#define _CATHBOARD_

#include "bitboard64.h"
#include "boardenums.h"
#include "move.h"

namespace cath {
    
    class ChessBoard
    {
    private:
        enum enumPiece {
            NONE, w_PAWN, b_PAWN, w_ROOK, b_ROOK, w_KNIGHT, b_KNIGHT,
            w_BISHOP, b_BISHOP, w_QUEEN, b_QUEEN, w_KING, b_KING
        };

        // Array for easily finding out which piece is at a given index
        enumPiece pieces[64];
        
        /* The bitboards for storing the positions of all pieces
         * Each bitboard is in a LERF ordering (start at a1 with index 0.
         * then go up rank by rank)
         * In each bitboard a 1 means that a piece exists at the square at that
         * index and a 0 that there is no piece there.
         */
        // Bitboards storing white pieces
        Bitboard64 w_Pawns, w_Rooks, w_Knights, w_Bishops, w_Queens, w_Kings;
        
        // Bitboards storing black pieces
        Bitboard64 b_Pawns, b_Rooks, b_Knights, b_Bishops, b_Queens, b_Kings;

        // Bitboards storing all attacked and occupied squares
        Bitboard64 w_All, w_Atk, b_All, b_Atk;

        // Array containing pointers to all piece-specific bitboards for looping
        Bitboard64* bitBoards[12] = {
            &w_Pawns, &b_Pawns, &w_Rooks, &b_Rooks, &w_Knights, &b_Knights,
            &w_Bishops, &b_Bishops, &w_Queens, &b_Queens, &w_Kings, &b_Kings
        };

        // Booleans indicating if white or black king is in check
        bool w_IsInCheck, b_IsInCheck;

        // Booleans indicating if castling on one side or the other is possible
        bool w_IsLeftCastleble, w_IsRightCastleble, b_IsLeftCastleble, b_IsRightCastleble;

        // Arrays for initializing the game
        const std::uint64_t w_InitPawns   = 0x00ff000000000000;
        const std::uint64_t w_InitRooks   = 0x8100000000000000;
        const std::uint64_t w_InitKnights = 0x4200000000000000;
        const std::uint64_t w_InitBishops = 0x2400000000000000;
        const std::uint64_t w_InitQueens  = 0x1000000000000000;
        const std::uint64_t w_InitKings   = 0x0800000000000000;
        
        const std::uint64_t b_InitPawns   = 0x000000000000ff00;
        const std::uint64_t b_InitRooks   = 0x0000000000000081;
        const std::uint64_t b_InitKnights = 0x0000000000000042;
        const std::uint64_t b_InitBishops = 0x0000000000000024;
        const std::uint64_t b_InitQueens  = 0x0000000000000010;
        const std::uint64_t b_InitKings   = 0x0000000000000008;

        // Last move (for en-passant)
        Move lastMove;

        // Second-to-last move (for repetition draws)
        Move secondMove;

        // Array of movement directions
        const int moveDirections[9] = {
            +7, +8, +9,
            -1, +0, +1,
            -9, -8, -7
        };

        /* 
         * The distance to the edge of the board from any square
         * the second array is structured as
         * [0] -> distance north
         * [1] -> distance sout
         * [2] -> distance east
         * [3] -> distance west
         */
        inline static int s_SquaresToEdge[64][4];
    
    private:
        /* Function to be called each time a piece is moved */
        void onMove (enumSquare from, enumSquare to, ChessBoard::enumPiece piece);

        /* Check if there are two pieces occupying the same square anywhere */
        bool checkForDoubles ();

        /*
         * Move a piece from one square to the other, regardless of if the move
         * is valid
         */
        void move (enumSquare pos1, enumSquare pos2);

        /*
         * Move a piece from one square to the other, regardless of if the move
         * is valid
         * 
         * Uses 2d indeces (rank and file)
         */
        void move (int rank1, int file1, int rank2, int file2);

        /*
         * Resets cursor to beginning of Board
         * (This assumes that the last thing that was done was print the board)
         */
        void resetCursor ();

        /*
         * Decode an instruction
         * instruct -> player's instrcution
         * file1 -> integer into which to store file of first position
         * rank1 -> integer into which to store rank of first position
         * file2 -> integer into which to store file of second position
         * rank2 -> integer into which to store rank of second position
         */
        void decodeInstruction (const char* instruct, enumSquare& pos1, enumSquare& pos2);

        /*
         * Generate pseudo-legal orthogonal sliding moves for a pice on a
         * certain square
         */
        Bitboard64 getSlidingMovesOrthogonal (enumSquare square, bool isWhite) const;

        /*
         * Generate pseudo-legal diagonal sliding moves for a pice on a certain
         * square
         */
        Bitboard64 getSlidingMovesDiagonal (enumSquare square, bool isWhite) const;

        /* Generate pseudo-legal moves for pawns */
        Bitboard64 getPawnMoves (enumSquare square, bool isWhite) const;

        /* Generate pseudo-legal moves for knight */
        Bitboard64 getKnightMoves (enumSquare square, bool isWhite) const;

        /* Generate pseudo-legal moves for kings */
        Bitboard64 getKingMoves (enumSquare square, bool isWhite) const;

        /* Generate legal moves for the pice on a certain square */
        Bitboard64 getLegalMoves (int file, int rank, enumPiece piece) const;

        /* Generate legal moves for the pice on a certain square */
        Bitboard64 getLegalMoves (enumSquare square) const;

        /* Calculate values for s_SquaresToEdge */
        void precomputeSquaresToEdge();

    public:
        /* Default Constructor. Initilizes positions to beginning of a game */
        ChessBoard ();

        /* Constructor using a FEN-string */
        ChessBoard (const char* fenString);

        /* Print a representation of the board to the console*/
        void print ();

        /* Print the piece on a certain square */
        void print (enumSquare square);

        /* Funktion for testing board updating */
        void experimentalmove ();
    };
} 

#endif // _CATHBOARD_
