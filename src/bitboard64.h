/*
 * Declaration of the cath::Bitboard64 struct
 */

#pragma once
#ifndef _CATHBITBOARD_
#define _CATHBITBOARD_

#include <cstdint>  // uint64_t
#include <iosfwd>

//typedef unsigned char BYTE;

namespace cath {

    /* 64-bit bitboard */
    struct Bitboard64
    {
        std::uint64_t m_Board;

        /*
         * Default Constructor
         *
         * Sets each value to 0
         */
        Bitboard64 ();

        /*
         * Constructor using a std::uint64_t
         * (This is the prefferred method as it is the fastest)
         */
        Bitboard64 (const std::uint64_t value);

        /* Constructor using eight chars */
        Bitboard64 (const unsigned char bytes[8]);

        /* Copy Constructor */
        Bitboard64 (const Bitboard64& original);

        /* Bitwise XOR two Bitboards */
        Bitboard64 operator^ (Bitboard64 other) const;

        /* Bitwise XOR equals */
        void operator^= (const Bitboard64 other);

        /* Bitwise AND two Bitboards */
        Bitboard64 operator& (Bitboard64 other) const;

        /* Bitwise AND equals */
        void operator&= (const Bitboard64 other);

        /* Bitwise OR two Bitboards */
        Bitboard64 operator| (Bitboard64 other) const;

        /* Bitwise OR equals */
        void operator|= (const Bitboard64 other);

        /* Bitwise NOT */
        Bitboard64 operator~ () const;

        /* Equals operator */
        bool operator== (Bitboard64 other) const;

        /* Does not equal operator */
        bool operator!= (Bitboard64 other) const;

        /* Get the state of the bit at index */
        bool operator[] (int index) const;

        /*
         * Get the state of the bit at index
         * Identical to operator[]
         */
        bool state (int index) const;

        /* Invert the bit at index */
        void invert (const int index);

        /* Set the bit at index to true */
        void set (const int index);

        /* Reset the bit at index to false */
        void reset (const int index);

        /* Return the total number of set bits */
        int quantity ();
    };

    /*
     * Return the total number of set bits in a Bitbaord64
     * This is somewhat slower than calling Bitboard64::quantity(), since the
     * input gets copied, but sometimes this may be necesarry
     */
    int quantity (const Bitboard64 board);
}

/* 
 * Overriede << operator for std::ostream object so that a cath::Bitboard64 can be
 * printed
 */
std::ostream& operator<< (std::ostream& stream, const cath::Bitboard64& bitboard);

#endif // _CATHBITBOARD_
