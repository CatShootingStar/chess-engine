/*
 * Implementation of the cath::ChessBoard class
 *
 * TODO:
 *   Replace all occurances of LOG() with more readable code
 */
#include <iostream>
#include <string>
#include <string.h>
#define _IOSTREAM_

#include "chessboard.h"

#define LOG(x) std::cout << x

namespace cath {

    // === Private methods === //

    /* Function to be called each time a piece is moved */
    void ChessBoard::onMove (enumSquare from, enumSquare to, ChessBoard::enumPiece piece)
    {
        /*
         * Set the bits for the Bitboards specifying occupied and attacked
         * spaces
         *
         * TODO: As always, this should be done more efficiently
         */
        w_All = w_Pawns | w_Rooks | w_Knights | w_Bishops | w_Queens | w_Kings;  // Update w_All
        b_All = b_Pawns | b_Rooks | b_Knights | b_Bishops | b_Queens | b_Kings;  // Update b_All

        // Update scores 'n shit

        /*
         * Determin Check by seeing if one color's attack overlaps with the
         * other's kings
         */
        w_IsInCheck = quantity(w_Kings & b_Atk) > 0;
        b_IsInCheck = quantity(b_Kings & w_Atk) > 0;

        // Update the last moves
        secondMove = lastMove;
        lastMove = Move(from, to, piece == enumPiece::w_PAWN || piece == enumPiece::b_PAWN);

        /*
         * Update rook status
         * The reason that piece is checked too is that you could have a custom
         * game where there never was a rook in one of the corners
         */
        if (from == a1 && piece == w_ROOK)
            w_IsLeftCastleble = true;
        else if (from == h1 && piece == w_ROOK)
            w_IsRightCastleble = true;
        else if (from == a8 && piece == b_ROOK)
            b_IsLeftCastleble = true;
        else if (from == h8 && piece == b_ROOK)
            b_IsRightCastleble = true;
    }

    /* Check if there are two pieces occupying the same square anywhere */
    bool ChessBoard::checkForDoubles ()
    {
        /*
         * Bitwise AND all Bitboards together. Return the result as a
         * boolean. Thus if true is returned then there are two pieces
         * occupying the same square somewhere
         */

        Bitboard64 zeroBoard;

        return !((w_Pawns & w_Rooks & w_Knights & w_Bishops & w_Queens & w_Kings
            & b_Pawns & b_Rooks & b_Knights & b_Bishops & b_Queens & b_Kings)
            == zeroBoard
        );
    }

    /*
     * Move a piece from one square to the other, regardless of if the move
     * is valid
     */
    void ChessBoard::move (enumSquare pos1, enumSquare pos2)
    {
        enumPiece movePiece = pieces[pos1];   // Determine which type of piece should be moved so the appropriate bitboard can be selected quickly
        enumPiece otherPiece = pieces[pos2];

        // Print error message if no piece can be moved
        if (movePiece == NONE)
        {
            std::cout << "No piece to move!\033[A";
            return;
        }
        int var;

        // Update square from which to move
        bitBoards[movePiece - 1]->reset(pos1);
        pieces[pos1] = NONE;

        // Update square at target position
        bitBoards[movePiece - 1]->set(pos2);
        if (otherPiece != NONE)
            bitBoards[otherPiece]->reset(pos2);
        pieces[pos2] = movePiece;

        onMove(pos1, pos2, movePiece);
    }

    /*
     * Move a piece from one square to the other, regardless of if the move
     * is valid
     *
     * Uses 2d indeces (rank and file)
     */
    void ChessBoard::move (int rank1, int file1, int rank2, int file2)
    {

    }

    /*
     * Resets cursor to beginning of Board
     * (This assumes that the last thing that was done was print the board)
     *
     * Currently only works on UNIX (possibly Mac, hasn't been tested yet)
     */
    void ChessBoard::resetCursor ()
    {
        std::cout << "\033[F\033[A\033[A\033[A\033[A\033[A\033[A\033[A" << std::flush;
    }

    /*
     * Decode an instruction
     *
     * instruct -> player's instrcution
     * file1 -> integer into which to store file of first position
     * rank1 -> integer into which to store rank of first position
     * file2 -> integer into which to store file of second position
     * rank2 -> integer into which to store rank of second position
     *
     * This needs to be reworked heavily to allow lax input styles by user
     * should also return some enum value indicating what to do, rather than
     * use the current outputs for that
     */
    void ChessBoard::decodeInstruction (const char* instruct, enumSquare& pos1, enumSquare& pos2)
    {
        // Integers representing squares
        int file1, rank1, file2, rank2;

        // quit -> exit the application (janky af)
        if(!strcmp(instruct, "quit"))
        {
            // quit the application
            pos1 = h8;
            pos2 = h8;
            return;
        }

        // Get the file1
        if (instruct[0] > 96 && instruct[0] < 105)
        {
            file1 = (int)instruct[0] - 97;
        }

        // Get the rank1
        if (instruct[1] > 48 && instruct[1] < 57)
        {
            rank1 = (int)instruct[1] - 49;
        }

        // Return no real move if syntax was improper
        if (!(instruct[2] == ',' && instruct[3] == ' '))
        {
            pos1 = (enumSquare)(file1 + rank1 * 8);
            pos2 = pos1;
            return;
        }

        // Get the file2
        if (instruct[4] > 96 && instruct[4] < 105)
        {
            file2 = (int)instruct[4] - 97;
        }

        // Get the rank2
        if (instruct[5] > 48 && instruct[5] < 57)
        {
            rank2 = (int)instruct[5] - 49;
        }

        // Convert the integer pairs to enumSquares
        pos1 = (enumSquare)(file1 + rank1 * 8);
        pos2 = (enumSquare)(file2 + rank2 * 8);
    }

    /* Generate legal orthogonal sliding moves for a pice on a certain square */
    Bitboard64 ChessBoard::getSlidingMovesOrthogonal (enumSquare square, bool isWhite) const
    {
        Bitboard64 moves;

        /* Loop through the diagonal directions
         * 0 -> Up
         * 1 -> Left
         * 2 -> Right
         * 3 -> Down
         */
        for (int i = 0; i < 4; ++i)
        {
            // Get the direction to move in
            int direction = moveDirections[2 * i + 1];
            int steps = 1;

            // Calculate which index of s_SquaresToEdge[...] to access
            int edgeIndex = (i % 2) * 2 + 2 * (i % 3) - 1 * (i % 4);

            /*
             * Go farther until either the edge of the board or a different
             * piece is met
             */
            while (true)
            {
                int nextPos = square + steps * direction;   // Calculate the index of the square ahead

                // Check if the edge of the board hasn't been hit yet
                if (s_SquaresToEdge[nextPos - direction][edgeIndex] > 0)
                {

                    /*
                     * Continue to the next direction if the next square is occupied
                     * by a piece of the same color
                     */
                    if (isWhite ? w_All[nextPos] : b_All[nextPos])
                    {
                        break;
                    }

                    /*
                     * Set the next square to true and continue to the next
                     * direction if it is occupied by a piece of the opposing
                     * color
                     * (This would translate to beating the piece and thus being
                     * unable to move any further)
                     */
                    if (isWhite ? b_All[nextPos] : w_All[nextPos])
                    {
                        moves.set(nextPos);
                        break;
                    }

                    // If we've gotten this far that means we can safely step onward
                    moves.set(nextPos);

                    ++steps;
                }
                // If the edge is met, continue to the next directions
                else
                    break;
            }
        }

        return moves;
    }

    /* Generate legal diagonal sliding moves for a pice on a certain square */
    Bitboard64 ChessBoard::getSlidingMovesDiagonal (enumSquare square, bool isWhite) const
    {
        Bitboard64 moves;

        /* Loop through the diagonal directions
         * 0 -> Up and left (+7)
         * 1 -> Up and rigt (+9)
         * 2 -> Down and left (-9)
         * 3 -> Down and right (-7)
         */
        for (int i = 0; i < 4; ++i)
        {
            // Some math to convert i to the index of the respective direction
            int direction = moveDirections[2 * (i / 2 + i)];
            int steps = 1;

            /*
             * Go farther until either the edge of the board or a different
             * piece is met
             */
            while (true)
            {
                int nextPos = square + steps * direction;   // Calculate the index of the square ahead

                // Check if the movement in the given direction is possible
                if (s_SquaresToEdge[nextPos - direction][i / 2] > 0
                    && s_SquaresToEdge[nextPos - direction][3 - i % 2] > 0)
                {
                    /*
                     * Continue to the next direction if the next square is occupied
                     * by a piece of the same color
                     */
                    if (isWhite ? w_All[nextPos] : b_All[nextPos])
                    {
                        break;
                    }

                    /*
                     * Set the next square to true and continue to the next
                     * direction if it is occupied by a piece of the opposing
                     * color
                     * (This would translate to beating the piece and thus being
                     * unable to move any further)
                     */
                    if (isWhite ? b_All[nextPos] : w_All[nextPos])
                    {
                        moves.set(nextPos);
                        break;
                    }

                    // If we've gotten this far that means we can safely step onward
                    moves.set(nextPos);

                    ++steps;
                }
                // If we hit the edge, continue with the next direction
                else
                    break;
            }
        }

        return moves;
    }

    /*
     * Generate legal moves for pawns
     * No check is made for pawns on the final row, since those can't exist
     */
    Bitboard64 ChessBoard::getPawnMoves (enumSquare square, bool isWhite) const
    {
        Bitboard64 moves;

        /*
         * Calculate the transformation for a one square move so that
         * if isWhite  -> 8
         * if !isWhite -> -8
         */
        int direction = (1 - !isWhite * 2) * 8;
        
        // Array of home rows indeces. First element is white, second is black
        int homeRows[2] = { 1, 6 };

        // Boolean indicating if the square ahead is occupied
        bool nextOccupied = (w_All | b_All)[square + direction];

        // Set the square ahead if it is empty
        if ( !nextOccupied )
            moves.set(square + direction);

        // Double move from home row
        bool isInHomeRow = (square / 8 == homeRows[!isWhite]);

        /*
         * Set the square two squares ahead to true if it isn't occupied and the
         * pawn is in its home row
         */
        if ( isInHomeRow && !((w_All | b_All)[square + direction * 2] || nextOccupied) )
            moves.set(square + direction * 2);

        /*
         * Check that the pawn could even move to either side
         * This prevents beating pieces acorss the board (e.g. a pawn on a3
         * could beat a piece on h4 if this check weren't in place)
         */
        bool canMoveLeft  = s_SquaresToEdge[square][3] > 0;
        bool canMoveRight = s_SquaresToEdge[square][2] > 0;

        /*
         * En-passant to the left
         * Set square ahead an to the left if it can move to the left and the
         * piece besides it can be beaten by an en-passant
         */
        if ( canMoveLeft && lastMove.to == square - 1 && lastMove.enPassantPossible )
            moves.set(square + direction - 1);

        // En-passant to the right
        if ( canMoveRight && lastMove.to == square + 1 && lastMove.enPassantPossible )
            moves.set(square + direction + 1);

        /*
         * Find the bitboard of the oppsoing color
         * Gets the address of the white bitboard and adds the distance to the
         * black bitboard if the color if white
         */
        Bitboard64 enemyBoard = *(&w_All + isWhite * (&b_All - &w_All));

        /*
         * Beating to the left
         * Sets square ahead and to the left if it can move to the left and
         * there is an enemy piece there
         * Ternary operator determines which color is the enemy color
         */
        if ( canMoveLeft && (enemyBoard[square + direction - 1]) )
            moves.set(square + direction - 1);

        // Beating to the right
        if ( canMoveRight && (enemyBoard[square + direction + 1]) )
            moves.set(square + direction + 1);

        return moves;
    }

    /* Generate pseudo-legal moves for knight */
    Bitboard64 ChessBoard::getKnightMoves (enumSquare square, bool isWhite) const
    {
        Bitboard64 moves;

        /*
         * Find the bitboard of the friendly color and the opposing attack
         */
        Bitboard64 enemyAtkBoard = *(&w_Atk + isWhite * (&b_Atk - &w_Atk));
        Bitboard64 friendlyBoard = *(&w_All + !isWhite * (&b_All - &w_All));
        
        // Hop, hop, aside
        // Up, up, left
        if ( !friendlyBoard[square + 15] && !enemyAtkBoard[square + 15]
            && s_SquaresToEdge[square][0] > 1 && s_SquaresToEdge[square][3] > 0 )
            moves.set(square + 15);

        // Up, up, right
        if ( !friendlyBoard[square + 17] && !enemyAtkBoard[square + 17]
            && s_SquaresToEdge[square][0] > 1 && s_SquaresToEdge[square][2] > 0 )
            moves.set(square + 17);
        
        // Left, left, up
        if ( !friendlyBoard[square + 6] && !enemyAtkBoard[square + 6]
            && s_SquaresToEdge[square][0] > 0 && s_SquaresToEdge[square][3] > 1 )
            moves.set(square + 6);

        // Right, right, up
        if ( !friendlyBoard[square + 10] && !enemyAtkBoard[square + 10]
            && s_SquaresToEdge[square][0] > 0 && s_SquaresToEdge[square][2] > 1 )
            moves.set(square + 10);

        // Left, left, down
        if ( !friendlyBoard[square - 10] && !enemyAtkBoard[square - 10]
            && s_SquaresToEdge[square][1] > 0 && s_SquaresToEdge[square][3] > 1 )
            moves.set(square - 10);

        // Right, right, down
        if ( !friendlyBoard[square - 6] && !enemyAtkBoard[square - 6]
            && s_SquaresToEdge[square][1] > 0 && s_SquaresToEdge[square][2] > 1 )
            moves.set(square - 6);

        // Down, down, left
        if ( !friendlyBoard[square - 17] && !enemyAtkBoard[square - 17]
            && s_SquaresToEdge[square][1] > 1 && s_SquaresToEdge[square][3] > 0 )
            moves.set(square - 17);

        // Up, up, right
        if ( !friendlyBoard[square - 15] && !enemyAtkBoard[square - 15]
            && s_SquaresToEdge[square][1] > 1 && s_SquaresToEdge[square][2] > 0 )
            moves.set(square - 15);
        
        return moves;
    }

    /* Generate pseudo-legal moves for kings */
    Bitboard64 ChessBoard::getKingMoves (enumSquare square, bool isWhite) const
    {
        Bitboard64 moves;

        /*
         * Find the bitboard of the friendly color and the opposing attack
         */
        Bitboard64 enemyAtkBoard = *(&w_Atk + isWhite * (&b_Atk - &w_Atk));
        Bitboard64 friendlyBoard = *(&w_All + !isWhite * (&b_All - &w_All));

        // Up and left
        if ( !friendlyBoard[square + 7] && !enemyAtkBoard[square + 7]
            && s_SquaresToEdge[square][0] > 0 && s_SquaresToEdge[square][3] > 0 )
            moves.set(square + 7);

        // Up
        if ( !friendlyBoard[square + 8] && !enemyAtkBoard[square + 8]
            && s_SquaresToEdge[square][0] > 0 )
            moves.set(square + 8);

        // Up and right
        if ( !friendlyBoard[square + 9] && !enemyAtkBoard[square + 9]
            && s_SquaresToEdge[square][0] > 0 && s_SquaresToEdge[square][2] > 0 )
            moves.set(square + 9);

        // Left
        if ( !friendlyBoard[square - 1] && !enemyAtkBoard[square - 1]
            && s_SquaresToEdge[square][3] > 0 )
            moves.set(square - 1);

        // Right
        if ( !friendlyBoard[square + 1] && !enemyAtkBoard[square + 1]
            && s_SquaresToEdge[square][2] > 0 )
        moves.set(square + 1);

        // Down and left
        if ( !friendlyBoard[square - 9] && !enemyAtkBoard[square - 9]
            && s_SquaresToEdge[square][1] > 0 && s_SquaresToEdge[square][3] > 0 )
        moves.set(square - 9);

        // Down
        if ( !friendlyBoard[square - 8] && !enemyAtkBoard[square - 8]
            && s_SquaresToEdge[square][1] > 0 )
            moves.set(square - 8);

        // Down and right
        if ( !friendlyBoard[square - 7] && !enemyAtkBoard[square - 7]
            && s_SquaresToEdge[square][1] > 0 && s_SquaresToEdge[square][2] > 0 )
            moves.set(square - 7);

        return moves;
    }

    /* Generate legal moves for the pice on a certain square */
    Bitboard64 ChessBoard::getLegalMoves (int file, int rank, enumPiece piece) const
    {
        enumSquare square = (enumSquare)(file + rank * 8);
        Bitboard64 moves;

        switch(piece)
        {
            case w_PAWN:
            {
                moves = getPawnMoves(square, true);
                break;
            }
            case b_PAWN:
            {
                moves = getPawnMoves(square, false);
                break;
            }
            case w_ROOK:
            {
                moves = getSlidingMovesOrthogonal(square, true);
                break;
            }
            case b_ROOK:
            {
                moves = getSlidingMovesOrthogonal(square, false);
                break;
            }
            case w_KNIGHT:
            {
                moves = getKnightMoves(square, true);
                break;
            }
            case b_KNIGHT:
            {
                moves = getKnightMoves(square, false);
                break;
            }
            case w_BISHOP:
            {
                moves = getSlidingMovesDiagonal(square, true);
                break;
            }
            case b_BISHOP:
            {
                moves = getSlidingMovesDiagonal(square, false);
                break;
            }
            case w_QUEEN:
            {
                moves = getSlidingMovesOrthogonal(square, true) |
                    getSlidingMovesDiagonal(square, true);
                break;
            }
            case b_QUEEN:
            {
                moves = getSlidingMovesOrthogonal(square, false) |
                    getSlidingMovesDiagonal(square, false);
                break;
            }
            case w_KING:
            {
                moves = getKingMoves(square, true);
                break;
            }
            case b_KING:
            {
                moves = getKingMoves(square, false);
                break;
            }
            default:
            {
                break;
            }
        }

        return moves;
    }

    /* Generate legal moves for the pice on a certain square */
    Bitboard64 ChessBoard::getLegalMoves (enumSquare square) const
    {
        return getLegalMoves(square % 8, square / 8, pieces[square]);
    }

    /* Calculate values for s_SquaresToEdge */
    void ChessBoard::precomputeSquaresToEdge()
    {
        // Iterate through all spaces of a chess board
        for (int rank = 0; rank < 8; ++rank)
        {
            for (int file = 0; file < 8; ++file)
            {
                // Calcualte distance to edge
                int toNorth = 7 - rank;
                int toSouth = rank;
                int toEast = 7 - file;
                int toWest = file;

                // Assign the values to the corresponding position in s_squaresToEdge
                s_SquaresToEdge[file + rank * 8][0] = toNorth;
                s_SquaresToEdge[file + rank * 8][1] = toSouth;
                s_SquaresToEdge[file + rank * 8][2] = toEast;
                s_SquaresToEdge[file + rank * 8][3] = toWest;
            }
        }
    }

    // === Public methods === //

    /* Default Constructor. Initilizes positions to beginning of a game */
    ChessBoard::ChessBoard ()
        : w_IsInCheck(false), b_IsInCheck(false),
          w_IsLeftCastleble(true), w_IsRightCastleble(true),
          b_IsLeftCastleble(true), b_IsRightCastleble(true),
          lastMove(a1, a1), secondMove(a1, a1),
          w_All(w_InitPawns)
    {
        // Initialize all pieces to their respective start of game positions
        //w_Pawns   = w_InitPawns;
        w_Rooks   = w_InitRooks;
        w_Knights = w_InitKnights;
        w_Bishops = w_InitBishops;
        w_Queens  = w_InitQueens;
        w_Kings   = w_InitKings;
        b_Pawns   = b_InitPawns;
        b_Rooks   = b_InitRooks;
        b_Knights = b_InitKnights;
        b_Bishops = b_InitBishops;
        b_Queens  = b_InitQueens;
        b_Kings   = b_InitKings;

        // Initialize pieces by looping through each piece-specific bitboard
        // Set everything to NONE
        for (int i = 0; i < 64; ++i)
        {
            pieces[i] = NONE;
        }
        // Then set a square to a piece if there is one
        for (int i = 0; i < 12; ++i)
        {
            for (int j = 0; j < 64; ++j)
            {
                if (bitBoards[i]->state(j))
                {
                    pieces[j] = (enumPiece)(i + 1);
                }
            }
        }

        // Initialize s_SquaresToEdge
        precomputeSquaresToEdge();
    }

    /* Constructor using a FEN-string */
    ChessBoard::ChessBoard (const char* fenString)
        : w_IsLeftCastleble(true), w_IsRightCastleble(true),
          b_IsLeftCastleble(true), b_IsRightCastleble(true),
          lastMove(a1, a1), secondMove(a1, a1)
    {
        // Interpret a fen string and initialize the Bitboards based on that

        // Update stuff
        //onMove();
    }

    /*
     * Print a representation of the board to the console
     * Currently only visualizes pawns because the default constructor is
     * fucking broken
     */
    void ChessBoard::print ()
    {
        // Print actual Board
        for (int rank = 0; rank < 8; ++rank)
        {
            for (int file = 0; file < 8; ++file)
            {
                int index = 8 * (7 - rank) + file;
                print ((enumSquare)index);
                LOG(" ");
            }
            LOG(std::endl);
        }
    }

    /* Print the piece on a certain square */
    void ChessBoard::print (enumSquare square)
    {
        // Print a character based on the piece at each square
        switch (pieces[square])
        {
            case NONE:
            {
                LOG("0");
                break;
            }
            case w_PAWN:
            {
                LOG("P");
                break;
            }
            case b_PAWN:
            {
                LOG("p");
                break;
            }
            case w_ROOK:
            {
                LOG("R");
                break;
            }
            case b_ROOK:
            {
                LOG("r");
                break;
            }
            case w_KNIGHT:
            {
                LOG("N");
                break;
            }
            case b_KNIGHT:
            {
                LOG("n");
                break;
            }
            case w_BISHOP:
            {
                LOG("B");
                break;
            }
            case b_BISHOP:
            {
                LOG("b");
                break;
            }
            case w_QUEEN:
            {
                LOG("Q");
                break;
            }
            case b_QUEEN:
            {
                LOG("q");
                break;
            }
            case w_KING:
            {
                LOG("K");
                break;
            }
            case b_KING:
            {
                LOG("k");
                break;
            }
            defualt:
            {
                LOG("?");
                break;
            }
        }
    }

    /* Plays a basic game and prints various debug messages
     * It's honestly really messy, especially on UNIX, but that's honestly not
     * really the point. Smooth UX will be a concern for when you can actually
     * play the game
     */
    void ChessBoard::experimentalmove ()
    {
        bool isOngoing = true;
        std::string input;

        enumSquare pos1, pos2;

        print();
        while (isOngoing)
        {
            // Recieve input
            getline(std::cin, input);
            std::cout << "\033[F";  // Reset cursor to input line

            decodeInstruction(input.c_str(), pos1, pos2);
            // Exit if poth pos1 and pos2 are h8
            if (pos1 == pos2 && pos1 == h8)
            {
                isOngoing = false;
                continue;
            }

            move(pos1, pos2);
            resetCursor();
            print();

            // Show next moves
            Bitboard64 nextMoves = getLegalMoves(pos2 % 8, pos2 / 8, pieces[pos2]);
            std::cout << "\n" << nextMoves << std::endl;
        }
    }
}
