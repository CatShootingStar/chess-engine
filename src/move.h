/*
 * Declaration of the chat::Move struct for storing moves
 */

#pragma once
#ifndef _CATHMOVE_
#define _CATHMOVE_

#include "boardenums.h"

namespace cath {

    /* Datatype for storing moves */
    struct Move
    {
        enumSquare from;
        enumSquare to;
        bool enPassantPossible;


        /*
         * Constructor
         *
         * first -> space that the piece was moved from
         * second -> space that the piece was moved to
         * assumes that the moved piece isn't a pawn
         */
        Move (const enumSquare first, const enumSquare second);

        /*
         * Constructor
         *
         * first -> space that the piece was moved from
         * second -> space that the piece was moved to
         * isPawn -> is the moved piece a pawn? (relevant for en-passant)
         */
        Move (const enumSquare first, const enumSquare second, const bool isPawn);

        /* Copy Constructor */
        Move (const Move& other);
    };
}

#endif // _CATHMOVE_
