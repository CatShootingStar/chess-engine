﻿# PowerShell Script for handling building and the like

# Create Debug directory and its subdirectories
function Setup {
    # If Debug directory doesn't exist create it and subdirectories
    if(-NOT(Test-Path .\Debug)) {
        New-Item -Path '.\Debug' -ItemType Directory
        New-Item -Path '.\Debug\bin' -ItemType Directory
        New-Item -Path '.\Debug\intermediate' -ItemType Directory
    }
    # If Debug exists only create subdirectories
    else {
        # Create the  Debug\bin  directory if it doesn't already exist
        if(-NOT(Test-Path .\Debug\bin)) {
            New-Item -Path '.\Debug\bin' -ItemType Directory
        }
        # Create the  Debug\intermediate  directory if it doesn't already exist
        if(-NOT(Test-Path .\Debug\intermediate)) {
            New-Item -Path '.\Debug\intermediate' -ItemType Directory
        }
    }
}

# Parse the input
# Use  .\build.ps1 help  to see options
switch($ARGS) {
    # Build project
    "build" {
        # Ensure that Debug Directory exists
        Setup
        # Compile project
        g++ -o .\Debug\bin\Chess.exe .\src\bitboard64.cpp .\src\move.cpp .\src\chessboard.cpp .\src\main.cpp -std=c++17
    }
    # Delete Debug directory
    "cleanup" {
        if(Test-Path .\Debug) {
            Remove-Item '.\Debug' -Recurse
        }
    }
    # Print help screen
    "help" {
        Write-Host("Usage: .\build.ps1 [options]")
        cat .\tools\help-build.txt
    }
    # Create Debug folder and its subfolders
    "setup" {
        Setup  
    }
    # Build test project (customize this for your needs)
    "testbuild" {
        # Ensure that Debug Directory exists
        Setup
        # Compile project
        g++ -o .\Debug\bin\test.exe .\src\bitboard64.cpp .\src\move.cpp .\src\chessboard.cpp .\src\test.cpp -std=c++17
    }
    # Return error if non existing command was used
    default {
        Write-Host("invalid input")
    }
}
