A Chess Engine with Python API
==============================

> Chess, one of man's greatest achievements, is quickly becoming more popular
> with each passing day.\
> We invite you to follow along as our scientists work to coneceive of a future,
> where chess is played not by man but by machine.\
> *♪ dum dum dum... dum-ba du-du-du-dum ♪*\
> \[Fun noise\]

[[_TOC_]]

About
-----

A ~~terrible~~ funky lil chess engine written in C++. Why? Cus it sounded like a
good and fun learning experience and I want to pit it against my friend's chess
machine (that's what I'm calling his machine learning algorythm that plays
chess).

Usage
-----

### Application ###

Note that board updating only works on Linux (or UNIX) at the moment, which
result on jankyness on Windows. I don't have a Mac to test this on, so i don't
know how anything behaves there.

Pieces can be moved by typing something that looks like `d2, d4`

Type `quit` to exit the application

### Code ###

A documentation is still work in progress.

Building
--------

### Windows ###

Run `.\build.ps1 build` in PowerShell. You can get an overview of the usage of
[`build.ps1`](build.ps1) by running `.\build.ps1 help`.

You may have to set your
[PowerShell exectuion policy](https://docs.microsoft.com/powershell/module/microsoft.powershell.core/about/about_execution_policies "Microsoft.PowerShell.Core > about_Execution_Policies")
in order to be able to run the script. `Set-ExecutionPolicy Unrestricted` is not
reccomended for security reasons but it is the most hassle-free way to get it to
work.

### Linux ###

Run `./build.sh build`. You can get an overview of the usage of
[`build.sh`](build.sh) by running `./build.sh help`.

In order to be able to execute the script run `chmod 755 build.sh`. If you have
experience with bash scripts you can of course do this however you like (and
correct my instructions), but if you don't then that's how to get it to run
easily and safely.

### Mac OSX ##

Apparently you should be able to follow the Linux instructions. If you have
trouble running it you could try replacing the shebang line (`#!/bin/bash`) in
`build.sh` with either `#!/bin/sh` or `#!/bin/zsh`.

Contibuting
-----------

uhhhhhhhhh\
This will be its own file once I've decided on how I want people to go about
contibuting. For now feel free to create issues, forks, and merge requests, etc.

If you have any questions feel free to contact me via
[Twitter](https://twitter.com/CatShootingStar) or
[email](mailto:catstar0077@gmail.com "catstar0077@gmail.com"). Note that email
is susceptible to being falsy identified as spam.
